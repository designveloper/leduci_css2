### CSS Essential 2  
1. CSS Selector:  
```  
  **CSS Comment**  
    - syntax: /* Your comment */
  **Add CSS**  
    There're 3 ways:  
    - External CSS:  
      + recommend  
      + separate with the files have .css extension  
      + reference files to <head> of HTML documents  
        <link rel="stylesheet" href="path"  
    - Internal CSS  
      + included in the <head>  
      + uses a <style> tag to enclose the CSS styles  
      + applies styles to only one page  
    - Inline CSS:  
      + using "style"
      + included in the HTML element as an attribute  
      + should be used sparingly and hard to maintain  
      + may cause specificity issues
  **Selector**  
    - Type Selectors:  
      + match the HTML by using the element name  
    - Class and ID Selector  
      .classname1.classname2 {
        /* applies to any element with the matching class (using for multiple pages) /*
      }
      #idname {
        /* applies to any element with the matching id */
      }  
    - Attribute Selectors:  
      [attr] { /* selects the matched attribute*/}  
      [attr=val] {/* selects the matched attribute and value */}  
    - Combinator selectors:  
      + Descendant Selectors:  
        parent child {}
        ancestor descentant {}  
      + Child combinators:  
        parent child {
          // child & descentant links  
        }  
        parent > child {
          // only child links  
        }  
      + Sibling combinators:  
        sibling1 + sibling2 { (+ = adjacent)
          // select only one - the sibling2 next to sibling1
        }  
        sibling1 ~ sibling2 { (~general)  
          // select all sibling type sibling2 next to sibling1  
        }  
      + Combinator selectors:  
          combine with comma  
          .class1.class2 { /*any element with both class names*/ }  
          element.classname { /* only paragraphs with a class of 'classname' */ }
          element#idname { /* only paragraphs with an id of idname*/ }  
    - Pseudo-class selectors:  
      + :first-child: selects the first child element of its parent  
      + :last-child: selects the last child element of its parent  
      + :first-of-type: selects the first child element of its type  
      + :last-child-type: selects the last child element of its type  
      + :nth-child(): select one or more child elements based on the order within the parent container.
      + :nth-child(keyword) - odd or even
      + :nth-child(number)  
      + :nth-child(algebraic expression) - a = any number, n and starts at a value of 0, b = any number  
      + :nth-of-type() same as nth-child  
    - Pseudo-element selectors:  
      + :before and :after  
```  
2. Layout:   
```  
  **The Box Model**  
    - width, height: sets specific size fro the content box  
    - padding: space inside of the element  
    - border: displays between the padding and margin  
    - margin: space outside of the element  
    - The box model fix:  
      html {
        box-sizing: border-box;  
      }  
      *, *:before, *:after {
        box-sizing: inherit;
      }  
  **Float and Display**  
    - float: align elements right or left (left/right)  
    - display:
      + inline-block  
      + block  
  **Position**  
    - to arrange elements relative to the default page flow or browser viewport  
    - values:
      + static: define the position of an element in the normal flow of the page.  
      + relative: establish a positioning context for its absolutely positioned descents, also for its self. (a positioning context is practically a coordinate system to determine the position of an element using the offset properties) => shift or nudge element from its original position using offset properties.  
      + absolute: relative to another element on the page.  
      + fix: relative to the viewport, removed from the flow of the page and doesn't affect the layout anymore.  
      + sticky/page/center
    - offset properties: top, right, bottom, left.
  NOTE:  
  Float vs Display vs position  
    - Float using when:  
      + variable and flexible content (e.g, image surrounded by text or blog posts with different lengths)  
      + global or large page structures (e.g, header, footer and sidebar)  
    - Display using when:  
      + aligning page component (make sure to account for the extra space)  
      + aligning elements that need to be center aligned  
      + doesn't change the page's natural page flow  
    - Position using when:  
      + positioning elements relative to another element  
      + aligning elements outside of the document flow  
      + positioning elements to a specific spot in the document  
    => Float, display, and position can't be used together on the same element.  
      + If using position, then float is ignored  
      + If using float, then display is ignored  
  **Layers and the z-index property**  
    - Stack content
    - z-index: using to locate position of relative element in stack content in z-axis  
```  
3. Tips and Tools:  
```  
  **Browser development tools**  
    - Develop Tools  
  **Debugging CSS**  
    - Check and uncheck properties  
  **Reseting stylesheets**  
    - Reset stylesheets: meyerweb.com
    - Normalize
  **Icon fonts**  
    - Web-safe fonts  
    - Web fonts  
  **The background property**  
      background-color  
      background-image  
      background-repeat  
      background-position (NEED declared if using background-size)
      background-attachment  
      background-size (NEED after background-position with /)
    shorthand: background  
  **Alpha transparency and gradients**  
    - RGB and AlphaTrnsparency  
      a => 0 = transparency, 1 = opaque  
    - linear-gradient(color, color) => one top, one underneath  
  ****
```  
4. Responsive design  
```  
  **Media Queries**  
    - used to specific how a document is presented on different media  
    - Media types:  
      + print  
      + speech  
      + screen  
      + all
    - Adding Media Queries:  
      + add to <link> with media  
      + add to CSS files using @media() {}
  ****
    - Progressive enhancement  
    - Graceful Degradation  
  **Device emulation**  
    - apply viewport meta tag
```  
